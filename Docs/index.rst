.. ATOM documentation master file, created by
   sphinx-quickstart on Sat Jan  9 16:02:48 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ATOM documentation
==================

.. toctree::
   :maxdepth: 1

   reference/index.rst
   tutorial/index.rst

