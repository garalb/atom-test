.. _introduction:


Tutorials
===========

This set of tutorials will guide you in the exploration of ATOM's
features

.. note::
   Input files have a .inp extension, and should be run using
   the ae.sh (all-electron), pg.sh (ps generation), or pt.sh (ps test)
   shell scripts in directory Utils. See the manual (Docs/atom.tex)
   for details.

   After the run, the output information and plotting scripts will
   reside in a sub-directory whose name is that of the input file
   without the .inp extension.

   Please refer to the user manual for the ATOM program for details on
   how to run the program and how to make sense of the output. 

   (** The aliases referred to in this section apply only
   to the "live" tutorials, or to users who have set up the
   aliases mentioned  ----
   For these exercises we have created the aliases ae, pg, and pt to
   perform all-electron, pseudopotential generation, and pseudopotential
   tests, respectively, and the alias gp to stand for gnuplot
   -persist. The alias energies, when used in the work directory, will
   show the inter-configuration energy changes (it is equivalent to grep
   "&d" OUT).  The aliases eigenvalues X, with X being s, p, d, will
   display the appropriate eigenvalues when applied to the OUT file.
   --- **)

..  toctree::
    :maxdepth: 1
    
    all-electron/index.rst
    ps-generation/index.rst

    
    

